---
layout: main
title: Jedo
---
# Jedo ORM Pages

Jedo is a lightweight Java Object-Relational Mapper (ORM).

## Design goals

Jedo's main design goals are:
* it should be as lightweight as possible
* it should be as orthogonal as possible
* it should be acceptably fast
Of course, a compromise sometimes have to be found between two or more of these goals.

### Lightweight

### Orthogonality
