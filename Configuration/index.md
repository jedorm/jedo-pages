---
layout: default
title: Configuration
---
# Jedo configuration

## Configuration file

The Jedo configuration file is an XML file conforming to the [Jedo configurationDTD](https://gitlab.com/jedorm/jedo/blob/master/src/main/resources/org/tastefuljava/jedo/conf/jedo-conf.dtd).

It defines a data source for the connection to the database, and specifies one or
more mapping file.

_Example_:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE configuration
        PUBLIC '-//tastefuljava.org//Jedo Configuration File 1.0//EN' 'jedo-conf.dtd'>
<configuration>
    <data-source>
        <property name="driver" value="org.h2.Driver"/>
        <property name="url" value="jdbc:h2:${user.home}/jedo-testdb/test"/>
        <property name="user" value="sa"/>
        <property name="password" value=""/>
    </data-source>
    <mapping resource="org/tastefuljava/jedo/mapping.xml"/>
</configuration>
```

If you application runs in a tomcat or some other application server, you can
define the data source properties in the configuration of that container; in that
case, the only property you need in the Jedo configuration file, is `jndi-name`.

## Mapping file

A Jedo mapping file is an XML file conforming to the [Jedo mapping DTD](https://gitlab.com/jedorm/jedo/blob/master/src/main/resources/org/tastefuljava/jedo/mapping/jedo.dtd)
